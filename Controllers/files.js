const formidable = require('formidable');
const path = require('path');
const fs = require('fs');
const db = require('../Models');
const archivo = db.files;

module.exports = {

//CARGAR ARCHIVOS AL SERVIDOR Y GUARDAR EN BASE DE DATOS
uploadFiles:(req, res, next) => {
    const dirname = path.join(__dirname, '../uploads');
    let extension = "";
    let file;

    const form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
        let file_body={
            imagen_name:file.name
        }
        archivo.create(file_body)
        .then(archivo => res.send({
            status: 200,
            file: archivo
           }))
        .catch(error => console.log(error));
        
    });

    //CAMBIAR EL NOMBRE ARCHIVO POR ORIGINAL
    form.on('fileBegin', function (name, f) {
    const length = f.name.length;
    const point = f.name.lastIndexOf(".");
    extension = f.name.slice(point + 1, length);
    let _name = f.name.slice(0, point);
    let idx = 0;
    while (idx !== -1) {
      idx = _name.indexOf(" ");
      _name = _name.replace(" ", "_")
    }
    idx = 0;
    while (idx !== -1) {
      idx = _name.indexOf("%");
      _name = _name.replace("%", "_")
    }
    console.log(_name);
    const date = new Date().getTime();
    f.name = _name + '_' + date + '.' + extension;
    f.path = dirname + '/' + f.name;
    file = f
    });

    form.on('file', function (name, file) {
        console.log('Uploaded ' + file.name);
    });

    form.on('end', function () {
       console.log('archivo subido con exito');
    });
},

//TRAER TODOS LOS ARCHIVOS

getListFiles: async (req, res) => {
    archivo.findAll()
    .then( archivo=> res.send(archivo))
    .catch(error => console.log(error));
},

//DELETE ARCHIVO SERVIDOR Y BASE DE DATOS
deleteFiles: (req, res,next) => {
  const dirname = path.join(__dirname, '../uploads');
  var data = req.body;

  fs.unlinkSync(dirname +'/'+ data.imagen_name);

  archivo.destroy({
      where: { id:data.id}
  })
      .then(archivo => res.send({
          status: 200,
          id: data.id
         }))
      .catch(error => console.log(error));
},

deleteFilesUploads: (req, res,next) => {
  const dirname = path.join(__dirname, '../uploads');
  var data = req.body.nombres;
  //console.log('json', data)
  for(var i=0;i<data.length;i++){
    fs.unlinkSync(dirname +'/'+ data[i].imagen_name);
    res.send({
        status: 200,
            })
  }
},
}