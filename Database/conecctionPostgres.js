// Conexion con postgreSQL usando sequelize como manipulador de la base de datos SQL
const Sequelize = require("sequelize");

// (Database(Name table), username, password) para establecer conexion con postgress mediante sequelize
const sequelize = new Sequelize('csr','postgres','postgres', {
  port: 5432,
  host: '127.0.0.1',
  dialect: "postgres",
});

module.exports = sequelize;
global.sequelize = sequelize;