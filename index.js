//SERVIDOR CON EXPRESS AND NODEMON
const express = require("express");
const bodyParser = require("body-parser");
const db = require("./Models");
const sql = require("./Database/conecctionPostgres");
const cors = require('cors');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



// Se agregan permisos de CORS
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    //res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Auth-Token, X-Requested-With, content-type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


// RUTAS PARA ACCEDER A LAS PETICIONES QUE SE HACEN EN EL CONTROLLER
// const routeImages = require("./Routes/images")(app);
const routeLogin = require("./Routes/login")(app);
const routeUsers = require("./Routes/Users")(app);
const routeFiles=require('./Routes/files')(app);

// SE USA PARA PODER VISUALIZR LAS IMAGENES SIN BLOQUEO 
app.use(express.static('uploads'));

// EXTABLECIENDO CONEXION CON POSTGRESQL (sequelize)
db.sequelize.sync().then(runserver);

function runserver() {
    app.listen(5000,() => {
        console.log("Connection Complete! 🚀");
    });    
}

// app.use('/', (req,res) => {
//     res.status(200).send('Servidor Corriendo con Nodemon')
// });

// app.listen(3000)





