const filesController = require("../Controllers/files");

module.exports = (app) =>
{
    app.post("/uploadFile",filesController.uploadFiles);
    app.get("/getListFiles", filesController.getListFiles);
    app.post("/deleteFiles", filesController.deleteFiles);
    app.post("/deleteFilesUploads", filesController.deleteFilesUploads);
  

}