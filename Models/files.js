const sequelize = require("sequelize");
const connection = require("../Database/conecctionPostgres");

module.exports = connection.define('archivos', {

	id:{
		type:sequelize.INTEGER,
		primaryKey:true,
		autoIncrement:true
	},
    imagen_name: {
		type: sequelize.STRING(255),
		allowNull: true,
		field: 'imagen_name'
	},


	}, {
		tableName: 'archivos'
	});